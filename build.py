#!/usr/bin/env python3

import bushi
import os
import shutil

try:

  libraries = [
    "bushi/__init__.py",
    "bushi/cmd.py",
    "bushi/io.py",
    "bushi/tmux.py"
  ]

  executables = [
    "bin/bushi-tmux-runner"
  ]

  work_path = os.path.dirname(os.path.join(os.getcwd(), __file__))
  os.chdir(work_path)

  bushi.io.process("cleaning package...")
  if os.path.isdir("package"):
    shutil.rmtree("package")

  bushi.io.process("copying *metadata*...")
  shutil.copytree("DEBIAN", "package/DEBIAN")
  bushi.io.success("*metadata* copied")

  bushi.io.process("copying libraries...")
  for library in libraries:
    try:
      bushi.io.process("copying *" + library + "*...")
      target = os.path.join("package/usr/lib/python3.6/dist-packages", library)

      if not os.path.isdir(os.path.dirname(target)):
        os.makedirs(os.path.dirname(target))
      shutil.copy(library, target)

    except FileNotFoundError:
      bushi.io.warn("cannot found *" + library + "*")

  bushi.io.success("*libraries* copied")

  bushi.io.process("copying executables...")
  for executable in executables:
    try:
      bushi.io.process("copying *" + executable + "*...")
      target = "package/usr/bin"
      if not os.path.isdir(target):
        os.makedirs(target)
      shutil.copy(executable, target)

    except FileNotFoundError:
      bushi.io.warn("cannot found *" + executable + "*")

  bushi.io.success("*executables* copied")

  bushi.io.process("building the *package*...")
  if not bushi.cmd.run_muted("dpkg-deb -b package"):
    bushi.io.terminate("failed to build the *package*")
  bushi.io.success("*package* built as *package.deb*")

  bushi.io.success("done")
  bushi.io.terminate()

except KeyboardInterrupt:
  bushi.io.terminate("keyboard interrupt")
